@extends('layouts.app')
@section('content')

<h1>This is the customers list</h1>
<a href="{{route('customers.create')}}">Create a New Customer </a>
<ul>
    @foreach($customers as $customer)
    @if($customer->status!=1)
    <li>
       name {{$customer->name}} &nbsp; email:{{$customer->email}}&nbsp;  phone:{{$customer->phone}}&nbsp; Sales Rep:{{$customer->user->name}}&nbsp;<a href="{{route('customers.edit',$customer->id)}}">Edit </a>
       @cannot('salesrep')  &nbsp;<a href="{{route('delete',$customer->id)}}">@endcannot Delete </a>&nbsp; @cannot('salesrep') <a href="{{route('closed',$customer->id)}}">@endcannot Deal Closed </a>
    </li>
    @else
    <li style="color:green">
       name {{$customer->name}}</li>
       <li> &nbsp;email:{{$customer->email}}&nbsp; phone:{{$customer->phone}}&nbsp; Sales Rep:{{$customer->user->name}}&nbsp; @cannot('salesrep')<a href="{{route('customers.edit',$customer->id)}}">@endcannot Edit </a>
       @cannot('salesrep') &nbsp; <a href="{{route('delete',$customer->id)}}">@endcannot Delete </a>
    </li>
    @endif

    @endforeach
</ul>
@endsection