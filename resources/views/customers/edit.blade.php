<h1>Edit Customers Details</h1>
<form method = 'post' action="{{action('CustomerController@update', $customers->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "name">Update Name</label>
    <input type= "text" class = "form-control" name= "name" value = "{{$customers->name}}">
</div>
<div class = "form-group">
    <label for = "email">Update Email</label>
    <input type= "text" class = "form-control" name= "email" value = "{{$customers->email}}">
</div>
<div class = "form-group">
    <label for = "phone">Update Phone</label>
    <input type= "text" class = "form-control" name= "title" value = "{{$customers->phone}}">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Update customer's details">
</div>

</form>
