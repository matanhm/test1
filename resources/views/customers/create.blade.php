@extends('layouts.app')
@section('content')

<h1>Create a New Customer</h1>
<form method = 'post' action="{{action('CustomerController@store')}}">
{{csrf_field()}}

<div class = "form-group">
    <label for = "name">Customer's Name</label>
    <input type= "text" class = "form-control" name= "name">
</div>
<div class = "form-group">
    <label for = "email">Customer's Email</label>
    <input type= "text" class = "form-control" name= "email">
</div>
<div class = "form-group">
    <label for = "phone">Customer's Phone</label>
    <input type= "text" class = "form-control" name= "phone">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Add Customer">
</div>

</form>
@endsection