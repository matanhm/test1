<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash; 
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                        'name' => 'a',
                        'email' => 'a@a.com',
                        'password' => Hash::make('12345678'),
                        'role' => 'manager',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                        'name' => 'b',
                        'email' => 'b@b.com',
                        'password' => Hash::make('12345678'),
                        'role' => 'salesrep',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                        'name' => 'c',
                        'email' => 'c@c.com',
                        'password' => Hash::make('12345678'),
                        'role' => 'salesrep',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
            
                    ]);
    }

}
